# ExTASY Workflows
====================

ExTASY is a tool to run multiple Molecular Dynamics simulations which can be 
coupled to an Analysis stage. This forms a simulation-analysis loop which can 
be made to iterate multiple times. It uses a pilot framework, namely Radical 
Pilot to run a large number of these ensembles concurrently on most of the 
commonly used supercomputers. The complications of resource allocation, data 
management and task execution are performed using Radical Pilot and handled 
by ExTASY.

# Documentation

Documentation can be found at http://extasy-workflows.readthedocs.org/en/latest/

#Build Status

[![Build Status](http://144.76.72.175:8080/buildStatus/icon?job=ExTASY-workflows)](http://144.76.72.175:8080/job/ExTASY-workflows/)

# Mailing List

* Users: https://groups.google.com/forum/#!forum/extasy-project
* Developers: https://groups.google.com/forum/#!forum/extasy-devel


For older issues/discussions/questions, please check the previous repository (deprecated): https://github.com/radical-cybertools/ExTASY.